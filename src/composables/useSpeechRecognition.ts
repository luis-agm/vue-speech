import { watch, ref, Ref } from "vue";

interface SpeechRecognitionHook {
  toggleListening: Function;
  note: Ref<string>;
  error: Ref<any>;
  isListening: Ref<boolean>;
}

declare global {
  interface Window {
    webkitSpeechRecognition: any;
    SpeechRecognition: any;
  }
}

const SpeechRecognition =
  window.SpeechRecognition || window.webkitSpeechRecognition;

const recognition = new SpeechRecognition();

recognition.continuous = true;
recognition.interimResults = true;
recognition.lang = "en_US";

export default function useSpeechRecognition(): SpeechRecognitionHook {
  const isListening = ref(false);
  const note = ref("");
  const error = ref(null);

  const toggleListening = (): void => {
    isListening.value = !isListening.value;
  };

  const handleListen = (): void => {
    if (isListening.value) {
      console.log("RECORDING START");
      recognition.start();
    } else {
      console.log("RECORDING STOP");
      recognition.stop();
    }
  };

  watch(isListening, (): void => {
    handleListen();
  });

  recognition.onresult = (evt: SpeechRecognitionEvent) => {
    const transcript = Array.from(evt.results)
      .map(result => result[0])
      .map(result => result.transcript)
      .join("");

    note.value = transcript;
  };

  recognition.onerror = function(this: SpeechRecognition, evt: any) {
    error.value = evt.error;
  };

  return {
    toggleListening,
    note,
    error,
    isListening
  };
}
